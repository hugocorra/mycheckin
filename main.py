# -*- coding: utf-8 -*-
import datetime

from bottle import route, run, template


checkin_file = 'checkin.csv'


def load_file(file_name):
    try:
        finput = open(file_name) 
    except IOError as e:
        return []

    rows = finput.readlines()

    for index, row in enumerate(rows):
        rows[index] = row.split(';')

    return rows


def save_file(file_name, rows):
    foutput = open(file_name, 'w')

    for row in rows:
        foutput.write(';'.join(row))

    foutput.close()


@route('/checkin')
def checkin():
    today = datetime.date.today().isoformat()
    time_now = datetime.datetime.now().time().isoformat().split('.')[0]

    rows = load_file(checkin_file)

    if rows and rows[-1] and rows[-1][0] == today:
        rows[-1].append(time_now)
    else:
        rows.append([today, time_now])

    save_file(checkin_file, rows)

    return '<html><body>Check-in feito: {} às {}</body></html>'.format(today, time_now)


@route('/list')
def list():
    rows = load_file(checkin_file)

    html = '<html><body><table border=1>'
    for day in rows:
        html += '<tr>'
        html += '<td>{}</td>'.format(day[0])
        for t in day[1:]:
            html += '<td>{}</td>'.format(t)
        html += '</tr>'

    html += '</table></body></html>'
    return html



run(host='localhost', port=8080)